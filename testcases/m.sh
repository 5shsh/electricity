#!/bin/sh

info() {
	echo "Starting to measure electricity"
	echo "Name:         $NAME"
	echo "Tariff:       $TARIFF"
	echo "Input Device: $INPUT_DEVICE"
	echo "MQTT_HOST:    $MQTT_HOST"
}

set_baud_rate() {
	#set $INPUT_DEV to 9600 8N1
	stty -F "$INPUT_DEVICE" 1:0:8bd:0:3:1c:7f:15:4:5:1:0:11:13:1a:0:12:f:17:16:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0
}

meter_output() {
	
	#METER_OUTPUT=`cat $INPUT_DEVICE 2>/dev/null | xxd -p -u -l 460`
	METER_OUTPUT=$(cat data3)

	
}

extract_value() {
	METER_HEX=$1
	METER_HEX_VAL=$(echo "${METER_HEX//[[:space:]]/}")
	#echo hy x${METER_HEX}x
	#echo hx x${METER_HEX_VAL}x
	let METER=0x${METER_HEX_VAL}
	EXTRACTED_VALUE=$(echo "scale=2; $METER / 10000" |bc)
}

METER_OUTPUT=''
EXTRACTED_VALUE=''