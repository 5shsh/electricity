# Electricity #

[![Docker Image Version](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fregistry.hub.docker.com%2Fv2%2Frepositories%2Fhomesmarthome%2Felectricity%2Ftags&query=%24.results%5B1%5D.name&colorB=4286f4)](https://microbadger.com/images/homesmarthome/electricity)

Gets the current electricity consumptiom (thanks to IR to USB Volkszähler) and sends the value as mqtt message. 

Environment file has to be specified: ```MQTT_HOST``` has to be set.

## Run (Example)
```
docker run -ti \
  -v $PWD/electricity/env:/env \
  -e NAME=electricity_general \
  -e INPUT_DEVICE=/dev/ttyUSB1 \
  --device=/dev/ttyUSB1 \
  homesmarthome/electricity:2.1.4 \
  /src/mt175.sh
```

## ED300L 
1.8.0 (total): 0x${METER_OUTPUT:315:10}
1.8.1: 0x${METER_OUTPUT:357:11} (get 11 chars because there is a space in between)
1.8.2: 0x${METER_OUTPUT:400:10}

https://wiki.volkszaehler.org/software/sml#beispiel_1emh_ed300l
https://www.ewh.de/fileadmin/user_upload/Stromnetz/Zaehlerstaende/Produktbeschreibung_EMH_ED300L_.pdf

## MT175
1.8.0: ${METER_OUTPUT:305:16}
2.8.0: ${METER_OUTPUT:357:11} (Einspeisung)
Wirkleistung: ${METER_OUTPUT:599:6}
