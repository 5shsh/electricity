#!/bin/sh

source /src/meter.sh

info
set_baud_rate
meter_output

extract_value "${METER_OUTPUT:305:16}"
METER_180=$EXTRACTED_VALUE
mosquitto_pub -h "$MQTT_HOST" -t "/electricity/status/$NAME/180" -m "{\"val\":$METER_180, \"tariff\": \"1.8.0\"}"

extract_value "${METER_OUTPUT:508:16}"
METER_280=$EXTRACTED_VALUE
mosquitto_pub -h "$MQTT_HOST" -t "/electricity/status/$NAME/280" -m "{\"val\":$METER_280, \"tariff\": \"2.8.0\"}"

extract_value "${METER_OUTPUT:599:6}"
METER_WIRKLEISTUNG=$EXTRACTED_VALUE
mosquitto_pub -h "$MQTT_HOST" -t "/electricity/status/$NAME/wirkleistung" -m "{\"val\":$METER_WIRKLEISTUNG}"

echo published.
sleep 300
