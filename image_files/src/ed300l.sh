#!/bin/sh

source /src/meter.sh

info
set_baud_rate
meter_output

extract_value "${METER_OUTPUT:315:10}"
METER_180=$EXTRACTED_VALUE
echo "$EXTRACTED_VALUE"
mosquitto_pub -h "$MQTT_HOST" -t "/electricity/status/$NAME/180" -m "{\"val\":$METER_180, \"tariff\": \"1.8.0\"}"

extract_value "${METER_OUTPUT:358:10}"
METER_181=$EXTRACTED_VALUE
echo "$EXTRACTED_VALUE"
mosquitto_pub -h "$MQTT_HOST" -t "/electricity/status/$NAME/181" -m "{\"val\":$METER_181, \"tariff\": \"1.8.1\"}"

extract_value "${METER_OUTPUT:400:10}"
METER_182=$EXTRACTED_VALUE
echo "$EXTRACTED_VALUE"
mosquitto_pub -h "$MQTT_HOST" -t "/electricity/status/$NAME/182" -m "{\"val\":$METER_182, \"tariff\": \"1.8.2\"}"

echo published.
sleep 300